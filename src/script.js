var distance = [];
var n = 0;
var i,j;

var cumulativeAccX = [];
var cumulativeAccY = [];
var cumulativeAccZ = [];

var totalAccX = 0;
var totalAccY = 0;
var totalAccZ = 0;

var vxn = 0, vxp = 0, vyn = 0, vyp = 0, vzn = 0, vzp = 0;
var dxn = 0, dxp = 0, dyn = 0, dyp = 0, dzn = 0, dzp = 0;

mapFrame.width = window.innerWidth;
mapFrame.height = window.innerHeight/2.4;

var bookedA = false;
var bookedB = false;
var bookedC = false;

var selectedDestination;

var pointOfInterest = [];

var distances = [];

window.addEventListener('devicemotion', function(e)
{
	n++;

	cumulativeAccX.push(e.acceleration.x);
	cumulativeAccY.push(e.acceleration.y);
	cumulativeAccZ.push(e.acceleration.z);

	if(n > 9)
	{
		for(i = n; i > n - 10; i--)
		{
			if(!isNaN(cumulativeAccX[i]))
			{
				totalAccX += cumulativeAccX[i];
			}
			if(!isNaN(cumulativeAccY[i]))
			{
				totalAccY += cumulativeAccY[i];
			}
			if(!isNaN(cumulativeAccZ[i]))
			{
				totalAccZ += cumulativeAccZ[i];
			}
		}
		totalAccX = totalAccX/10;
		totalAccY = totalAccY/10;
		totalAccZ = totalAccZ/10;

		vxn = vxp + (totalAccX*0.1);
		dxn = dxp + (vxn*0.1);
		
		vyn = vyp + (totalAccY*0.1);
		dyn = dyp + (vyn*0.1);

		vzn = vzp + (totalAccZ*0.1);
		dzn = dzp + (vzn*0.1);
	}
})

function directionMapping()
{
	var a = document.getElementById('destination');
	selectedDestination = a.options[destination.selectedIndex];
	if(selectedDestination.value != '')
	{
		var f = document.getElementById('mapFrame');
		f.hidden = false;

		var g = document.getElementById('upButton');
		g.hidden = false;
	}
	a.hidden = true;

	if(selectedDestination.value === 'pantry')
	{
		entranceM2.setAttribute('visible','true');
		leftPantry.setAttribute('visible','true');

		straightMeetingRoom.setAttribute('visible','false');
		rightRoom.setAttribute('visible','false');
		rightWashRoom.setAttribute('visible','false');
	}

	else if(selectedDestination.value ==='mRoom1')
	{
		straightMeetingRoom.setAttribute('visible','true');
		rightRoom.setAttribute('visible','true');

		entranceM2.setAttribute('visible','false');
		leftPantry.setAttribute('visible','false');
		rightWashRoom.setAttribute('visible','false');
	}

	else if(selectedDestination.value ==='washRoom')
	{
		entranceM2.setAttribute('visible','true');
		rightWashRoom.setAttribute('visible','true');

		leftPantry.setAttribute('visible','false');
		straightMeetingRoom.setAttribute('visible','false');
		rightRoom.setAttribute('visible','false');
	}

    cursor.setAttribute('visible','true');
}

document.querySelector('#moreInfo').addEventListener('click', function(e)
{
	var e = document.getElementById('destination');
	e.hidden = true;

	var f = document.getElementById('detailing');
	f.hidden = false;
})

document.querySelector('#toPoi').addEventListener('click', function(e)
{
	// var a = document.getElementById('trivia');
	// a.hidden = true;

	// var b = document.getElementById('seatCapacity');
	// b.hidden = true;

	// var c = document.getElementById('book');
	// c.hidden = true;

	// var d = document.getElementById('schedule');
	// d.hidden = true;

	var a = document.getElementById('detailing');
	a.hidden = true;

	var e = document.getElementById('destination');
	e.hidden = false;

	// if(selectedDestination.value != '')
	// {
	// 	var f = document.getElementById('mapFrame');
	// 	f.hidden = false;

	// 	var g = document.getElementById('upButton');
	// 	g.hidden = false;
	// }

	cursor.setAttribute('visible','false');
})

document.querySelector('#arrowStraight').addEventListener('click',function(e)
{
	alert("Hello");
})

document.getElementById('upButton').addEventListener('click',function(e)
{
	var a = document.getElementById('upButton');
	a.hidden = true;

	var b = document.getElementById('downButton');
	b.hidden = false;

	// var c = document.getElementById('book');
	// c.hidden = true;

	// var d = document.getElementById('seatCapacity');
	// d.hidden = true;

	// var e = document.getElementById('schedule');
	// e.hidden = true;

	// var f = document.getElementById('trivia');
	// f.hidden = true;

	mapFrame.height = window.innerHeight;
})

document.getElementById('downButton').addEventListener('click',function(e)
{
	var a = document.getElementById('downButton');
	a.hidden = true;

	var b = document.getElementById('upButton');
	b.hidden = false;

	mapFrame.height = window.innerHeight/2.4;
	cursor.setAttribute('visible','true');
})

function findDistance(lat1, lon1, lat2, lon2)
{
	var R = 6371; 					
  	var dLat = deg2rad(lat2-lat1);  
  	var dLon = deg2rad(lon2-lon1); 
  	var a = 
    	Math.sin(dLat/2) * Math.sin(dLat/2) +
    	Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    	Math.sin(dLon/2) * Math.sin(dLon/2); 
  	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  	var d = R * c; 					
  	return d;
}

function deg2rad(deg) 
{
  	return deg * (Math.PI/180)
}

function init()
{
	var jsonObjects = {"access_token":"eyJhbGciOiJSUzI1NiIsImtpZCI6IjhjOWViOTY4ZjczNzQ0ZWFlZDQyMWU0ODAxMDE0MmJjZTUxYTA2N2YifQ.eyJhenAiOiI1ODc1MDA3MjM5NzEtc2k4czRxcWF0OXY1ZWZnZW1uZWJpaHBpM3FlOW9ubHAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI1ODc1MDA3MjM5NzEtc2k4czRxcWF0OXY1ZWZnZW1uZWJpaHBpM3FlOW9ubHAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTA4ODUyODQyMDQ1NDAzMDIyMTUiLCJlbWFpbCI6InNyaXNoYWsubWFodWxpQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiRmJwSGE3UkQ4UE9MNkQ2Q01UVmlNZyIsImV4cCI6MTUzMTg5NzAzNCwiaXNzIjoiYWNjb3VudHMuZ29vZ2xlLmNvbSIsImp0aSI6IjFjZTkzN2RiNGQxZmMxNmEwOWE5NjZmYWQ2Yzg0ODU2YTVjZmI3ODEiLCJpYXQiOjE1MzE4OTM0MzQsIm5hbWUiOiJzcmlzaGEgbWFodWxpIiwicGljdHVyZSI6Imh0dHBzOi8vbGg1Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tUFozTVYtSFJSdE0vQUFBQUFBQUFBQUkvQUFBQUFBQUFIYnMvYUNOOHBvZEF5YWsvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6InNyaXNoYSIsImZhbWlseV9uYW1lIjoibWFodWxpIiwibG9jYWxlIjoiZW4tR0IifQ.p9WlsTvA9PKUcO_nJtX2tr8Hzib0Ru28QWcND4jLUSpDFezv9ggA1T2nOxsGHdbi_Jqqa4IuW5eUoU64pddvSB3AYg90pOdUiknCQ0fF_9cr6kAwrIExwBWRPmJLOxk0kCmuQ9P7AzcnDWByq2O6KynsoZbmAMjMThkkzU_4jfibB943_ocRtOh-RHeihnpC_SrlpL7AlytEBckCIRP4ze0ni6xVgr9CTFPLJ_lw80xYJsUB3uUi5FF0ihdkw0X53fkVb0jqjxdXtds5Sv0v9cUqYAOoA2INTV7FJ2jO0yb5OrZUSjccIitu7OEyyVpIXVflIC205EzDKpHeRzNvZg","buid":"building_569f202e-c692-4003-a237-37aab5a1883e_1527135706280"};

 	$.ajax(
 	{
        url: "https://ap.cs.ucy.ac.cy/anyplace/mapping/pois/all_building",
      	type:"POST",
      	data: JSON.stringify(jsonObjects),
      	dataType: "json",
      	contentType: "application/json; charset=utf-8",
      	success: function(result) 
      	{
        	result.pois.forEach(function(element)
        	{
        		pointOfInterest.push(element);
        	})
        	pointOfInterest.forEach(function(element)
    		{
    			pointOfInterest.forEach(function(element1)
    			{
    				if(element.puid != element1.puid)
    				{
    					var a = findDistance(element.coordinates_lat, element.coordinates_lon, element1.coordinates_lat, element1.coordinates_lon);
    					distances.push(a);
    					console.log(a);
    				}
    			})
    		})
      	}
    });

	document.getElementById("destination").options[0]=new Option("Please Select Destinations","");
	document.getElementById("destination").options[1]=new Option("Meeting Room","mRoom1");
	document.getElementById("destination").options[2]=new Option("Wash-Room","washRoom");
	document.getElementById("destination").options[3]=new Option("Pantry","pantry");
}